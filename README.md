# Hospital Management System
Hospital Management System using MySQL, Php and Bootstrap

##  Group Members:

### 1. Shaunak Halbe 111803053 
### 2. Siddhant Kandge 111803060
### Division 1

## Prerequisites
1. Install XAMPP web server


## Languages and Technologies used
1. HTML5/CSS3
2. JavaScript 
3. Bootstrap 
4. XAMPP 
5. Php
6. MySQL 
7. TCPDF

## Steps to run the project in your machine
1. Download and install XAMPP in your machine
2. Clone or download the repository
3. Extract all the files and move it to the 'htdocs' folder of your XAMPP directory.
4. Start the Apache and Mysql in your XAMPP control panel.
5. Open your web browser and type 'localhost/phpmyadmin'
6. In phpmyadmin page, create a new database from the left panel and name it as 'myhmsdb'
7. Import the file 'myhmsdb.sql' inside your newly created database and click ok.
8. Open a new tab and type 'localhost/foldername' in the url of your browser

    

  




