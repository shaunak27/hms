

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";




CREATE TABLE `admintb` (
  `username` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `Role` varchar(100)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `admintb` (`username`, `password`,`Role`) VALUES
('admin', 'admin123', NULL),('owner','owner123','Hospital Owner');


CREATE TABLE `doctb` (
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `spec` varchar(50) NOT NULL,
  `docFees` int(10) NOT NULL,
  PRIMARY KEY(`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `doctb` (`username`, `password`, `email`, `spec`, `docFees`) VALUES
('ashok', 'ashok123', 'ashok@gmail.com', 'General', 500),
('arun', 'arun123', 'arun@gmail.com', 'Cardiologist', 600),
('Dinesh', 'dinesh123', 'dinesh@gmail.com', 'General', 700),
('Ganesh', 'ganesh123', 'ganesh@gmail.com', 'Pediatrician', 550),
('Kumar', 'kumar123', 'kumar@gmail.com', 'Pediatrician', 800),
('Amit', 'amit123', 'amit@gmail.com', 'Cardiologist', 1000),
('Abbis', 'abbis123', 'abbis@gmail.com', 'Neurologist', 1500),
('Tiwary', 'tiwary123', 'tiwary@gmail.com', 'Pediatrician', 450);


CREATE TABLE `patreg` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `password` varchar(30) NOT NULL,
  `cpassword` varchar(30) NOT NULL,
  `weight` int(3),
  `dob` date,
  PRIMARY KEY(`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `patreg` (`pid`, `fname`, `lname`, `gender`, `email`, `contact`, `password`, `cpassword`) VALUES
(1, 'Ram', 'Kumar', 'Male', 'ram@gmail.com', '9876543210', 'ram123', 'ram123'),

(10, 'Peter', 'Norvig', 'Male', 'peter@gmail.com', '9609362815', 'peter123', 'peter123');



CREATE TABLE `appointmenttb` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `ID` int(11) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `doctor` varchar(30) NOT NULL,
  `docFees` int(5) NOT NULL,
  `appdate` date NOT NULL,
  `apptime` time NOT NULL,
  `userStatus` int(5) NOT NULL,
  `doctorStatus` int(5) NOT NULL,
  PRIMARY KEY(`ID`),
  FOREIGN KEY (`pid`)
        REFERENCES patreg(`pid`)
        ON DELETE CASCADE,
  FOREIGN KEY (`doctor`)
        REFERENCES doctb(`username`)
        ON DELETE CASCADE          
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `bill` (
  `bill_no` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `doctor_fees` int(11) NOT NULL DEFAULT 0,
  `lab_fees` varchar(11) NOT NULL DEFAULT 0,
  `fixed_charges` varchar(100) NOT NULL DEFAULT 800,
  `total_fees` varchar(11) NOT NULL DEFAULT 800,
  
  PRIMARY KEY(`bill_no`),
  FOREIGN KEY (`pid`)
        REFERENCES patreg(`pid`)
        ON DELETE CASCADE        
) ENGINE=InnoDB ;

CREATE TABLE `lab` (
  `lab_id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `doc_id` varchar(30) NOT NULL ,
  `domain` varchar(25) NOT NULL ,
  `test` varchar(25) NOT NULL ,
  `fees` int(11) DEFAULT 300,
  
  PRIMARY KEY(`lab_id`)     
) ENGINE=InnoDB ;

CREATE TABLE `labappointmenttb` (
  `ID` int(100) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `test` varchar(25) NOT NULL,
  `labFees` int(5) NOT NULL ,
  `appdate` date NOT NULL,
  `apptime` time NOT NULL,
  `userStatus` int(5) NOT NULL,
  `testStatus` int(5) NOT NULL,
  PRIMARY KEY(`ID`),
  FOREIGN KEY (`pid`)
        REFERENCES patreg(`pid`)
        ON DELETE CASCADE     
) ENGINE=InnoDB ;

CREATE TABLE `contact` (
  `name` varchar(30) NOT NULL,
  `email` varchar(20) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `message` varchar(200) NOT NULL,
  PRIMARY KEY(`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `contact` (`name`, `email`, `contact`, `message`) VALUES
('Anu', 'anu@gmail.com', '7896677554', 'Hey Admin'),
(' Viki', 'viki@gmail.com', '9899778865', 'Good Job, Pal'),
('Ananya', 'ananya@gmail.com', '9997888879', 'How can I reach you?'),
('Aakash', 'aakash@gmail.com', '8788979967', 'Love your site'),
('Mani', 'mani@gmail.com', '8977768978', 'Want some coffee?');



CREATE TABLE `prestb` (
  `doctor` varchar(50) NOT NULL,
  `pid` int(11) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `appdate` date NOT NULL,
  `apptime` time NOT NULL,
  `disease` varchar(250) NOT NULL,
  `allergy` varchar(250) NOT NULL,
  `prescription` varchar(1000) NOT NULL,
  PRIMARY KEY(`ID`),
  FOREIGN KEY (`pid`)
        REFERENCES patreg(`pid`)
        ON DELETE CASCADE,
  FOREIGN KEY (`doctor`)
        REFERENCES doctb(`username`)
        ON DELETE CASCADE      
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `prestb` (`doctor`, `pid`, `ID`, `fname`, `lname`, `appdate`, `apptime`, `disease`, `allergy`, `prescription`) VALUES

('Tiwary', 9, 13, 'Peter', 'Norvig', '2020-03-26', '14:00:00', 'Cough', 'Skin dryness', 'Intake fruits with more water content');


COMMIT;


